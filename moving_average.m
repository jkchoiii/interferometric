function y=moving_average(A,XX,Res)

[Nx,Ny]=size(A);
y=zeros(Nx,Ny);

WLeng=round(Res/(XX(2,1)-XX(1,1)))+1;
if mod(WLeng,2)==0
    WLeng=WLeng+1;
end

Eind=[1:Nx]+(WLeng-1)/2;
Sind=Eind-WLeng;

for n=1:Ny
    for p=1:Nx
        y(p,n)=mean(A(max(1,Sind(p)):min(Eind(p),Nx),n));
    end
end
