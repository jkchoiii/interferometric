function [outputArg1,outputArg2] = SASProcessing(Pingindex, data, XT, XA, vt, Dt, Ntrunc, Chn, fc, fs, Wav, Wavnum, rows, cols)

ping_interval = Pingindex(2)-Pingindex(1);
Endping = Pingindex(end);
Sensor = 1:32;

for m=1:ping_interval:Endping

    
    % Location of Tx and Rx
    XT=XT+vt*Dt(m);
    XA=XA+vt*Dt(m);
        
    % import Raw data
    stbdsig=double(data.port.real_up((Sensor) + (m-1)*32,:)-8192)/2048+1j*double(data.port.imag_up((Sensor) + (m-1)*32,:)-8192)/2048;
    stbdsig=stbdsig(:,1:Ntrunc); % truncation of data
    
    %if (m>=1)&(any(Pingindex==mm))
          
        %stbdsig=stbdsig(:,1:Ntrunc); % truncation of data
      
         if m==1
             NNt=2^nextpow2(size(stbdsig,2)); % nearest power of 2
             ts=[0:size(stbdsig,2)-1]/fs;
             
             %Ymax=ts(end)*cw/2;
             b_sig=TxReplica_final(NNt,Chn,fc,fs,Wav,Wavnum); % Replica of Tx signal in frequency domain (basebanding, undersampled)
             
             % For vernier array
             NewXA=XA; % location of Rx
             NewXT=XT;
             XAA=NewXA;
             XTA=NewXT;
             
             % DCPA
             NewVXA=(NewXA+NewXT)/2; % Vernier array
             VXA=NewVXA;
             
             % Vernier
             stbdsig_ver=stbdsig;
             Newsig_ver=stbdsig_ver;
             
             Sping=m;
         else
             OldXA=NewXA;
             OldXT=NewXT;
             OldVXA=NewVXA;
             
             NewXA=XA; % location of Rx
             NewXT=XT;
             
             % DCPA
             NewVXA=(NewXA+NewXT)/2; % Vernier array
             
             Prevind=find(NewVXA<max(OldVXA)); % From DPCA
             Nextind=find(NewVXA>=max(OldVXA)); % From DPCA
             
             % Vernier
             XAA=[XAA NewXA(Nextind)]; 
             XTA=[XTA NewXT(Nextind)];
             
             % DCPA
             VXA=[VXA NewVXA(Nextind)];
             
             Newsig_ver=stbdsig;
             stbdsig_ver=[stbdsig_ver; Newsig_ver(Nextind,:)];
         end
        
        pij1=zeros(rows, cols);

        % Matched filtering
        if Wav=='W'
            MSTBDsig(1+(m-1)*Chn:m*Chn,:)=Mfilter_final(b_sig,stbdsig); % For CW only
        elseif Wav=='P'
            MSTBDsig(1+(m-1)*Chn:m*Chn,:)=stbdsig; 
        end
        
        % Choose signals
        Pimagestbd1=TDBF_SAS_final(Chn,cw,D_t,D_ap,fc,vt,0,ts,MSTBDsig(1+(m-1)*Chn:m*Chn,:),XT,XA,YT,YA,ZT0,ZA0,XX1,YY1,pij1);
        %Pimagestbd1=TDBF_SAS_final(Chn,cw,fc,vt,0,ts,MSTBDsig(1+(m-1)*Chn:m*Chn,:),XT,XA,XX1,YY1,pij1);

        Pimage1=Pimage1+Pimagestbd1; % for single side
       
        FigHandle = figure(1);
        set(FigHandle, 'Position', [500, 500, 1249, 400]);
        imagesc(abs(Pimage1)/max(max(abs(Pimage1))), 'Xdata', [Ymin1 Ymax1], 'Ydata', [Xmin1 Xmax1]); % for single sides
        caxis([0 0.1])
       
        xlabel('Range (m)','FontSize',12)
        ylabel('Along-track (m)','FontSize',12)
        set(gca,'XDir','norm','YDir','norm')
        colormap(mycolor)
        drawnow;
    %end
end


end

