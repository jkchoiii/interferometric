function y=TDBF_SAS_final(Nu, cw,D_t,D_ap,center_freq,vtx,vty,ts,sig, XT,XA, ...
                                                                            YT,YA,ZT, ZA, ...
                                                                                          XX, YY, pij0)
%                        (Chn,cw,D_t,D_ap,fc,         vt, 0,  ts,data,XT,XA,YT,YA,ZT0,ZA0,XX1,YY1,pij1);
k0=2*pi*center_freq/cw;
%center_freq=600e3;

vtx=vtx/cw;
vty=vty/cw;
v=sqrt(vtx^2+vty^2); % towing speed ratio

for n = 1:Nu
   % Bistatic 
     %dr = sqrt((XA(n)-XX).^2 + (YA(n)-YY).^2+ZA^2);
     %dt = sqrt((XT(n)-XX).^2 + (YT(n)-YY).^2+ZT^2);
     dr = sqrt((XA(n)-XX).^2 + (YA-YY).^2+ZA^2);
     dt = sqrt((XT(n)-XX).^2 + (YT-YY).^2+ZT^2);
     %dst=dr+dt;
     %dst=(dt-vt/cw*(XX-XA(n))+sqrt(dr.^2+((vt/cw)^2)*(dt.^2-dr.^2+(XX-XA(n)).^2)-2*vt/cw*(XX-XA(n)).*dt))/(1-(vt/cw)^2);
     %dst=dt-(XX-XA(n))*vtx-(YY-YA(n))*vty+sqrt(((XX-XA(n))*vtx+(YY-YA(n))*vty-dt).^2-(1-v^2)*(dt.^2-dr.^2));
     dst=dt-(XX-XA(n))*vtx-(YY-YA)*vty+sqrt(((XX-XA(n))*vtx+(YY-YA)*vty-dt).^2-(1-v^2)*(dt.^2-dr.^2));
     dst=dst/(1-v^2);
     
     %thetat=atan2(XX-XT(n),YY-YT(n));
     %thetar=atan2(XX-XA(n),YY-YA(n));
     
     thetat=atan2(XX-XT(n),YY-YT);
     thetar=atan2(XX-XA(n),YY-YA);
     %azg=(abs(thetat)<2.5*pi/180);
     
     At=sin(k0/(2*pi)*D_t*sin(thetat))./(k0/(2*pi)*D_t*sin(thetat)); % Tx-beam pattern
     At(thetat==0)=1;
     
     Ar=sin(k0/(2*pi)*D_ap*sin(thetar))./(k0/(2*pi)*D_ap*sin(thetar)); % Rx-beam pattern
     Ar(thetar==0)=1;
     
     tmp = abs(At.*Ar).*interp1(ts, sig(n,:),dst/cw,'spline',0).*exp(1j*2*pi*center_freq*dst/cw);
     %tmp = azg.*interp1(ts, sig(n,:),dst/cw,'spline',0).*exp(1j*2*pi*center_freq*dst/cw);
     %tmp = At.*Ar.*interp1(ts, sig(n,:),dst/cw,'spline',0).*exp(1j*2*pi*center_freq*dst/cw);
     %tmp = interp1(ts,sig(n, :),dst/cw,'spline',0).*exp(1j*2*pi*center_freq*dst/cw);
     %tmp=filter(0.45,[1 1-0.45],tmp,[],1); % Gaussian exponential filter
     %tmp=Gaussfilter(tmp);
     %size(tmp)
     pij0 = pij0 + tmp;   
end



y=pij0.*YY/max(YY(1,:)); % TVG
%y=pij0;

























    
    
    
    
    
    
    



