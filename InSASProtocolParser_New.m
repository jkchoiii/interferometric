function [data, IMU,data_range] = InSASProtocolParser_New()

record_count = zeros(9,1);
record_data_size = cell(9,1);
%dir_path = 'C:\Users\jkchoi\Documents\MATLAB\여러가지\테스트';
%file_ext = '*.sona';
%file_list = dir(fullfile(dir_path, file_ext));
[file,path] = uigetfile('C:\Users\user\Documents\MATLAB\data\insas\*.insas','Save data As');

fid = fopen(fullfile(path, file), 'r');
fseek(fid, 0, 'eof');
file_length = ftell(fid);
fseek(fid, 0, 'bof');

SHORT_BYTE = int64(2);

disp('load data')
tic
data = fread(fid, file_length, 'uint8=>uint8');
% DVL=DVLParse(path, file);
%IMU=IMUParse(path, file);
 IMU=0;
toc
fclose(fid);

data_record_frame_size = 24;
current_idx = int64(1);

store_idx = ones(2,1);
SONAR_DATA = cell(10000,1);
ch_count = zeros(10000,1);
data_count = zeros(10000,1);
% GPS_DATA = cell(10000,1);

% h = waitbar(0, 'parse');
disp('parse data')

length(data)

tic
while current_idx < length(data)
%     waitbar(double(current_idx)/double(length(data)))
    data_record_frame = typecast(data(current_idx:current_idx+data_record_frame_size-1), 'int32');
    current_idx = current_idx + data_record_frame_size;
    
    if(data_record_frame(end)==100)
        
        %%
        record_count(1) = record_count(1) + 1;
        record_header_size = 100;        
        
        record_header = typecast(data(current_idx:current_idx+record_header_size-1), 'int32');
                
        Year = record_header(3);
        Month = record_header(4);
        Day = record_header(5);
        Hour = record_header(6);
        Minute = record_header(7);
        Seconds = typecast(fliplr(data(int64((0:3)+7*4)+current_idx)), 'single');
        SonarTime(store_idx(1))=posixtime(datetime(Year,Month,Day,Hour,Minute,Seconds));
        
        if store_idx(1)==1           
            data_count = record_header(1);
            ch_count = record_header(2);
            data_range=record_header(16);
        end
        current_idx = current_idx + record_header_size;
        
        %%
        num_of_data = int64(record_header(1)*record_header(2)*2);
        record_data_size{1} = [record_data_size{1} num_of_data];
        
        SONAR_DATA{store_idx(1)} = typecast(data(current_idx:current_idx+num_of_data*SHORT_BYTE-1), 'int16');
        
        store_idx(1) = store_idx(1)+1;
        current_idx = current_idx + num_of_data*SHORT_BYTE;
    else
        current_idx = current_idx + int64(data_record_frame(4));
    end
end
toc

sonar_data = SONAR_DATA(1:(store_idx(1)-1),1);
clearvars -except knots sonar_data ch_count data_count data_range SonarTime IMU

tmp_data = cell(length(sonar_data),1);
port = [];
port.real_up = tmp_data;
port.imag_up = tmp_data;
port.real_down = tmp_data;
port.imag_down = tmp_data;
stbd = [];
stbd.real_up = tmp_data;
stbd.imag_up = tmp_data;
stbd.real_down = tmp_data;
stbd.imag_down = tmp_data;
data = [];
data.port = port;
data.stbd = stbd;

% h = waitbar(0, 'align');
disp('align data')

port_real_up_index=zeros(1,ch_count(1)*data_count(1)/4);
port_imag_up_index=zeros(1,ch_count(1)*data_count(1)/4);
stbd_real_up_index=zeros(1,ch_count(1)*data_count(1)/4);
stbd_imag_up_index=zeros(1,ch_count(1)*data_count(1)/4);

port_real_down_index=zeros(1,ch_count(1)*data_count(1)/4);
port_imag_down_index=zeros(1,ch_count(1)*data_count(1)/4);
stbd_real_down_index=zeros(1,ch_count(1)*data_count(1)/4);
stbd_imag_down_index=zeros(1,ch_count(1)*data_count(1)/4);

for n=1:data_count(1)
port_real_up_index((1:ch_count(1)/4)+(n-1)*(ch_count(1)/4))=(1:2:ch_count(1)/2)+(n-1)*ch_count(1)*2;
port_imag_up_index((1:ch_count(1)/4)+(n-1)*(ch_count(1)/4))=(1:2:ch_count(1)/2)+1+(n-1)*ch_count(1)*2;
port_real_down_index((1:ch_count(1)/4)+(n-1)*(ch_count(1)/4))=(1:2:ch_count(1)/2)+ch_count(1)/2+(n-1)*ch_count(1)*2;
port_imag_down_index((1:ch_count(1)/4)+(n-1)*(ch_count(1)/4))=(1:2:ch_count(1)/2)+1+ch_count(1)/2+(n-1)*ch_count(1)*2;

stbd_real_up_index((1:ch_count(1)/4)+(n-1)*(ch_count(1)/4))=(1:2:ch_count(1)/2)+ch_count(1)+(n-1)*ch_count(1)*2;
stbd_imag_up_index((1:ch_count(1)/4)+(n-1)*(ch_count(1)/4))=(1:2:ch_count(1)/2)+1+ch_count(1)+(n-1)*ch_count(1)*2;
stbd_real_down_index((1:ch_count(1)/4)+(n-1)*(ch_count(1)/4))=(1:2:ch_count(1)/2)+ch_count(1)/2+ch_count(1)+(n-1)*ch_count(1)*2;
stbd_imag_down_index((1:ch_count(1)/4)+(n-1)*(ch_count(1)/4))=(1:2:ch_count(1)/2)+1+ch_count(1)/2+ch_count(1)+(n-1)*ch_count(1)*2;
end


tic
for idx = 1:length(sonar_data)
%     waitbar(idx / length(sonar_data));
    cur_data = sonar_data{idx};    
    data.port.real_up{idx} = reshape(cur_data(port_real_up_index), [ch_count(1)/4 data_count(1)]);
    data.port.imag_up{idx} = reshape(cur_data(port_imag_up_index), [ch_count(1)/4 data_count(1)]);
    data.port.real_down{idx} = reshape(cur_data(port_real_down_index), [ch_count(1)/4 data_count(1)]);
    data.port.imag_down{idx} = reshape(cur_data(port_imag_down_index), [ch_count(1)/4 data_count(1)]);
    
    data.stbd.real_up{idx} = reshape(cur_data(stbd_real_up_index), [ch_count(1)/4 data_count(1)]);
    data.stbd.imag_up{idx} = reshape(cur_data(stbd_imag_up_index), [ch_count(1)/4 data_count(1)]);
    data.stbd.real_down{idx} = reshape(cur_data(stbd_real_down_index), [ch_count(1)/4 data_count(1)]);
    data.stbd.imag_down{idx} = reshape(cur_data(stbd_imag_down_index), [ch_count(1)/4 data_count(1)]);
end
data.port.real_up = cell2mat(data.port.real_up);
data.port.imag_up = cell2mat(data.port.imag_up);
data.port.real_down = cell2mat(data.port.real_down);
data.port.imag_down = cell2mat(data.port.imag_down);

data.stbd.real_up = cell2mat(data.stbd.real_up);
data.stbd.imag_up = cell2mat(data.stbd.imag_up);
data.stbd.real_down = cell2mat(data.stbd.real_down);
data.stbd.imag_down = cell2mat(data.stbd.imag_down);

data.time = SonarTime;

toc
% close(h)
clearvars -except knots data_range data IMU

end
