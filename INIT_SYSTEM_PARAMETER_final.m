function [fc,fs,c,Chn,D_t,D_ap,dx,XT,XA,YT,YA,ZT,ZA,Xc,Yc,Zc,odx,ody,vt,Wav,Wavnum,UD] = INIT_SYSTEM_PARAMETER_final()
vt=0.2;

% Frequency & sampling parameters:
c = 1500;           % Sound speed
fc=400e3; % center frequency
fs=100e3; % sampling frequency

% Array parameters:
dx = 0.04;          % Spacing between Rx array
Chn=32; % number of array element in towfish
D_t=0.22; % size of Tx
D_ap=1*dx; % size of aperture

XA=[0:Chn-1]*dx; % location of Rx
XT=((Chn-1)/2*dx+1.056)*ones(size(XA)); % location of Tx
%XT=((Chn-1)/2*dx-1.056)*ones(size(XA)); % location of Tx

%Xoffset=(2605.87/2-375.2)*1e-3; % distance from the center of mass to the nearest Rx to Tx

YA=zeros(size(XA));

%XA=XA-Xoffset;
%XT=XT-Xoffset;

% coordinates of center of mass
Xc=0;
Yc=0;
Zc=0;

%odx=2e-2; % along-track resolution
%ody=2.1e-2; % range resolution
odx=1e-2;
ody=1e-2;

% Wav='W'; % CW mode
% Wavnum=40e-6; % Tx mode number
Wav='P'; % CP mode
%Wavnum=250e-6;
Wavnum=1e-6;

UD='U';

ZT=0;
ZA=0;
YT=0;
YA=0;


% if fc==200e3
%     ZT=-125.53e-3;
%     YT=(81.75e-3)*ones(size(XA));
%     if UD=='U'
%         ZA=42.59e-3;
%         YA=(146.13e-3)*ones(size(XA));
%     elseif UD=='D'
%         ZA=-125.53e-3;
%         YA=(81.75e-3)*ones(size(XA));
%     end
% elseif fc==400e3
%     ZT=42.59e-3;
%     YT=(146.13e-3)*ones(size(XA));
%     if UD=='U'
%         ZA=42.59e-3;
%         YA=(146.13e-3)*ones(size(XA));
%     elseif UD=='D'
%         ZA=-125.53e-3;
%         YA=(81.75e-3)*ones(size(XA));
%     end
% end
    



