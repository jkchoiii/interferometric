function y=TxReplica_final(NNt,Chn,fc,fs,Wav,Wavnum)

% make replica

Delt=1/fs;
ts=[0:NNt-1]*Delt;

if Wav=='W'
    %b_sig=(ts<=Wavnum/fc);
    b_sig=(ts<=Wavnum);
    %b_sig=b_sig.*exp(-1j*2*pi*(600e3)*ts);
elseif Wav=='P'
%     if Wavnum==25
%         TD=250e-6; % chirp duration
%     elseif Wavnum==50
%         TD=500e-6;
%     end
    Bw=50e3; % Bandwidth 50 kHz
%     b_sig=exp(1j*(2*pi*(Bw/(2*TD)*ts.^2-0.5*Bw*ts)));
%     b_sig=b_sig.*(ts<=TD);
    b_sig=exp(1j*(2*pi*(Bw/(2*Wavnum)*ts.^2-0.5*Bw*ts)));
    b_sig=b_sig.*(ts<=Wavnum);
    %b_sig=b_sig.*exp(-1j*2*pi*fc*[0:length(b_sig)-1]*Delt);
end
y=fftshift(fft(fftshift(repmat(b_sig,Chn,1),2),[],2),2);   
%y=fftshift(fft(fftshift(repmat([b_sig zeros(1,NNt-length(b_sig))],Chn,1),2),[],2),2);   

