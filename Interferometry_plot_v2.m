clear; clc; close all;

%% initialize
obj = SASBathymetry();


%% Set full coord.
Yarea = [0; 250];
Xarea = [0; (obj.params.array.Chn-1)*obj.params.array.dx + obj.params.array.vt*obj.params.coord.Dt(1)*(obj.params.ping_length-1)];
[obj.params.sas.fullSAS] = obj.SetSASCoordinate(Yarea, Xarea, obj.params.coord.ody, obj.params.coord.odx);

%% full range SAS
%[obj.params.sas.fullSAS] = obj.CallSASProcessingOnline(obj.data.up, obj.params.sas.fullSAS);

%% plot full range SAS image 
figure;
%imagesc(abs(obj.params.sas.fullSAS.Pimage), ...     % full sas to region sas
imagesc(abs(obj.data.up(:,1:obj.params.sas.fullSAS.Ntrunc)), ... % raw-data to region sas
    'Xdata', [obj.params.sas.fullSAS.Ymin obj.params.sas.fullSAS.Ymax], ...
    'Ydata', [obj.params.sas.fullSAS.Xmin obj.params.sas.fullSAS.Xmax]);
xlabel('Range (m)','FontSize',12)
ylabel('Along-track (m)','FontSize',12)
set(gca,'XDir','norm','YDir','norm'); %colormap(mycolor);
drawnow; disp('Click area of finer SAS')

[Yarea, Xarea] = ginput(2); % determine starting and ending coordinates of selected signal

%% Set range params for selected region.

[obj.params.sas.SAS.params] = obj.SetSASCoordinate(Yarea, Xarea, obj.params.coord.ody/4, obj.params.coord.odx/4);

%% selected region SAS
[obj.params.sas.SAS.up] = obj.CallSASProcessingOnline(obj.data.up, obj.params.sas.SAS.params);
[obj.params.sas.SAS.down] = obj.CallSASProcessingOnline(obj.data.down, obj.params.sas.SAS.params);

% normalize
[obj.params.sas.SAS.up.Pimage] = obj.params.sas.SAS.up.Pimage ./ max(max(abs(obj.params.sas.SAS.up.Pimage)));
[obj.params.sas.SAS.down.Pimage] = obj.params.sas.SAS.down.Pimage ./ max(max(abs(obj.params.sas.SAS.down.Pimage)));

%% plot selected region SAS image 
figure;
imagesc(abs(obj.params.sas.SAS.up.Pimage), ...
    'Xdata', [obj.params.sas.SAS.params.Ymin obj.params.sas.SAS.params.Ymax], ...
    'Ydata', [obj.params.sas.SAS.params.Xmin obj.params.sas.SAS.params.Xmax]);
xlabel('Range (m)','FontSize',12)
ylabel('Along-track (m)','FontSize',12)
set(gca,'XDir','norm','YDir','norm'); %colormap(mycolor);
drawnow;

% Select target bathymetry area
disp('Click two ends of the first target to along-track direction ')
[Yalong,Xalong]=ginput(2); % determine starting and ending coordinates of selected signal

disp('Click maximum range of target to cross-track direction ')
[Ycross,Xcross]=ginput(1);

%% apply cutoff threshold
cutoff = 0.2;
[obj.params.sas.SAS.up.Pimage] = obj.PreprocessImage(obj.params.sas.SAS.up.Pimage, cutoff);
[obj.params.sas.SAS.down.Pimage] = obj.PreprocessImage(obj.params.sas.SAS.down.Pimage, cutoff);

%% get bathymetry image
[obj.params.sas.SAS.bathy] = obj.Bathymetry(obj.params.sas.fullSAS, obj.params.sas.SAS, Yalong);

%% plot bathymetry image
figure;
imagesc(obj.params.sas.SAS.bathy, ...
    'Xdata', [obj.params.sas.SAS.params.Ymin, obj.params.sas.SAS.params.Ymax], ...
    'Ydata', [obj.params.sas.SAS.params.Xmin, obj.params.sas.SAS.params.Xmax])
xlabel('Range (m)','FontSize',12)
ylabel('Along-track (m)','FontSize',12)
set(gca,'XDir','norm','YDir','norm')
drawnow




%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[yyy, xxx] = ginput(2);

HeightSAS=Heightest.*abs(PPimage(:,:,1)>=Cutoff1);
HeightSAS = HeightSAS.*abs((XX2<=max(xxx))&(XX2>=min(xxx)));


figure(6)
imagesc(HeightSAS,'Xdata', [Ymin2 Ymax2], 'Ydata', [Xmin2 Xmax2]);
colorbar
set(gca,'XDir','norm','YDir','norm')
xlabel('Range (m)','FontSize',12)
ylabel('Along-track (m)','FontSize',12)


% Target range setting
HeightSAS=HeightSAS.*(YY2<=Ylim);
HeightSAS=HeightSAS.*((XX2<=max(Xcoor))&(XX2>=min(Xcoor)));

figure(99);
HeightSAS = HeightSAS.*(HeightSAS>0);
valid_yy = find(any(HeightSAS));
valid_xx = find(any(HeightSAS.'));
target_max_range = YY2(1,valid_yy(end)) - YY2(1,valid_yy(1));
target_max_height = XX2(valid_xx(end),1) - XX2(valid_xx(1),1);
new_yy = 0:ody/4:target_max_range;
new_xx = 0:ody/4:target_max_height;
XXX = repmat(new_xx.', 1, length(new_yy));
YYY = repmat(new_yy, length(new_xx), 1);
new_HeightSAS = HeightSAS(valid_xx(1):valid_xx(1)+length(new_xx)-1,valid_yy(1):valid_yy(1)+length(new_yy)-1);
figure; plot3(YYY(find(new_HeightSAS)), XXX(find(new_HeightSAS)),new_HeightSAS(find(new_HeightSAS)), '.')
grid on
grid minor
axis([0 new_yy(end) 0 new_xx(end) 0 1.2])
xticks((0:0.1:1.7))
zticks((0:0.1:1.5))
xlabel('Range (m)','FontSize',12)
ylabel('Along-track (m)','FontSize',12)
zlabel('Estimated Height (m)','FontSize',12)
toc()
view(0,0)

figure(7)
plot3(YY2(find(HeightSAS)),XX2(find(HeightSAS)),HeightSAS(find(HeightSAS)),'.')
axis([Ymin2 Ymax2 Xmin2 Xmax2 0 1.2])
grid on
xlabel('Range (m)','FontSize',12)
ylabel('Along-track (m)','FontSize',12)
zlabel('Estimated Height (m)','FontSize',12)
toc()
view(0,0)


figure(8)
plot3(YY2(find(HeightSAS)),XX2(find(HeightSAS)),extracted_height,'.')
axis([Ymin2 Ymax2 Xmin2 Xmax2 0 1.2])
grid on
grid minor 
xlabel('Range (m)','FontSize',12)
ylabel('Along-track (m)','FontSize',12)
zlabel('Estimated Height (m)','FontSize',12)
toc()

view(0,0)

