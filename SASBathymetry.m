classdef SASBathymetry

    properties
        params
        data
    end
    
    methods
        function obj = SASBathymetry()
            params = [];
            params.cw = 1500;
            params.fc = 400e3;
            params.fs = 100e3;
            params.depth = 7.78; % depth of water tank
            params.delu = 25e-2;
            
            params.array = [];
            params.array.vt = 0.2; % m/s
            params.array.Chn = 32;
            params.array.Sensor = 1:params.array.Chn;
            params.array.dx = 0.04;
            params.array.D_t = 0.22;
            params.array.D_ap = 1*params.array.dx;
            params.array.XA = (params.array.Sensor-1)*params.array.dx;
            params.array.XT = ((params.array.Chn-1)/2*params.array.dx+1.056)*ones(size(params.array.XA));
            params.array.baseline = 18e-2;
            
            params.pulse = [];
            params.pulse.Wav = 'P';
            params.pulse.Wavnum = 200e-6;
            
            params.coord = [];
            params.coord.Xc = 0;
            params.coord.Yc = 0;
            params.coord.Zc = 0;
            
            params.coord.odx = 1e-2;
            params.coord.ody = 1e-2;
            params.coord.ZT = 0;
            params.coord.ZA = 0;
            params.coord.YT = 0;
            params.coord.YA = 0;
            
            %%
            
            [data, ~, ~]=InSASProtocolParser_New();
            
            ping_length = length(data.time);
            
            
            Nskip = round(0.8/params.array.vt);
            if Nskip < 1
                Nskip = 1;
            end
            valid_ping = 1:Nskip:ping_length; % 이 핑에 해당하는 데이터만 처리
            params.Nskip = Nskip;
            sonar_time = data.time(1:ping_length);
            dt = mean(diff(sonar_time));
            Dt = dt*ones(size(sonar_time));
            
            params.coord.Dt = Dt;
            
            params.sas.SAS = [];
            params.sas.SAS.params = [];
            params.sas.SAS.up = [];
            params.sas.SAS.down = [];
            params.sas.fullSAS = [];            
            
%             params.sas.fullSAS.Ymin = 0;
%             params.sas.fullSAS.Ymax = 30;
%             params.sas.fullSAS.Xmin = 0;
%             params.sas.fullSAS.Xmax = (params.array.Chn-1)*params.array.dx + params.array.vt*dt*(ping_length-1);
%             
%             Yran = params.sas.fullSAS.Ymin:params.coord.ody:params.sas.fullSAS.Ymax;
%             Xran = params.sas.fullSAS.Xmin:params.coord.odx:params.sas.fullSAS.Xmax;
%             
%             params.sas.fullSAS.XX = repmat(Xran', 1, length(Yran));
%             params.sas.fullSAS.YY = repmat(Yran, length(Xran), 1);
%             params.sas.fullSAS.Pimage = zeros(length(Xran), length(Yran));
            
%             params.Ntrunc = 2*round(sqrt(params.sas.fullSAS.Ymax^2+(params.sas.fullSAS.Xmax/2)^2)/params.cw*params.fs);
%             if params.Ntrunc > size(data.port.real_up, 2)
%                 params.Ntrunc = size(data.port.real_up, 2);
%             end
            %
            
            params.full_data_count = size(data.port.real_up, 2);
            obj.params = params;
            
            obj.params.ping_length = ping_length;
            tmp_data = [];
            tmp_data.up = [];
            tmp_data.down = [];
            tmp_data.up = double(data.port.real_up-8192)/2048+1j*double(data.port.imag_up-8192)/2048;
            tmp_data.down = double(data.port.real_down-8192)/2048+1j*double(data.port.imag_down-8192)/2048;
            obj.data = tmp_data;
            
        end
        function [sas_params] = SetSASCoordinate(obj, Yarea, Xarea, ody, odx)
            sas_params = [];
            sas_params.ody = ody;
            sas_params.odx = odx;
            
            % Select area that target is imaged.
            sas_params.Xmin = min(Xarea);
            sas_params.Xmax = max(Xarea);
            sas_params.Ymin = min(Yarea);
            sas_params.Ymax = max(Yarea);
            Yran = sas_params.Ymin:ody:sas_params.Ymax;
            Xran = sas_params.Xmin:odx:sas_params.Xmax;
            sas_params.XX = repmat(Xran',1,length(Yran));
            sas_params.YY = repmat(Yran,length(Xran),1);
            sas_params.Pimage = zeros(length(Xran), length(Yran));
            
            sas_params.Ntrunc = 2*round(sqrt(sas_params.Ymax^2+(sas_params.Xmax/2)^2)/obj.params.cw*obj.params.fs);
            
            if sas_params.Ntrunc > obj.params.full_data_count
                sas_params.Ntrunc = obj.params.full_data_count;
            end

            
        end
        

        function sas_params = CallSASProcessingOnline(obj, data, sas_params)
            
            num_sensors = obj.params.array.Chn;
            %% init으로 옮길 수 있을듯
            XTmat = zeros(length(obj.params.coord.Dt), length(obj.params.array.XT));
            XAmat = zeros(length(obj.params.coord.Dt), length(obj.params.array.XT));
            
            XT = obj.params.array.XT;
            XA = obj.params.array.XA;
            
            %init = false;
            for idx = 1:length(obj.params.coord.Dt)
                XT = XT + obj.params.array.vt*obj.params.coord.Dt(idx);
                XTmat(idx,:) = XT .* ones(1,length(obj.params.array.XT));
                
                XA = XA + obj.params.array.vt*obj.params.coord.Dt(idx);
                XAmat(idx,:) = XA .* ones(1,length(obj.params.array.XA));
            end
            
            %NNt = 2^nextpow2(obj.params.Ntrunc);
            ts = (0:sas_params.Ntrunc-1)/obj.params.fs;
            
            
            %%
            tic
            for idx = 1 : obj.params.Nskip : obj.params.ping_length
                disp(num2str((idx/obj.params.ping_length)*100));
                
                data_rows = 1+(idx-1)*num_sensors:(idx-1)*num_sensors+32;
                data_piece = data(data_rows,1:sas_params.Ntrunc);
                
                [ obj, sas_params ] = ProcessSAS(obj, ts, data_piece, XTmat(idx,:), XAmat(idx,:), sas_params);
                
                %VernierDPCA(data, XT, XA, init);
            end
            toc
            
        end
        
        function [ height_est ] = Bathymetry(obj, fullSAS, SAS, Y_along)
            
            Ystart = min(Y_along);
            fc = obj.params.fc;
            fs = obj.params.fs;
            cw = obj.params.cw;
            D = obj.params.depth;
            d = obj.params.array.baseline;
            cross_range = fullSAS.YY(1,:);
            full_SR = sqrt(cross_range.^2-D^2); % slant-range
            
            cross_range_phase = angle(exp(1j*2*pi*fc*2*cross_range/cw));
            unwraped_cross_range_phase = unwrap(cross_range_phase,[],2);
            background = unwraped_cross_range_phase / (2*pi*fc/cw*d)*1.1325.*full_SR.*(cross_range>=D);
            
            dy = Ystart - SAS.params.Ymin; % 현재 선택한 새로운 타겟 시작 지점 - 고해상도 sas 시작 지점
            num_queries = floor(dy/SAS.params.ody);
            query_pts = linspace( SAS.params.Ymin, Ystart, num_queries); % 타겟 시작 지점부터 정밀하게 페이즈 찾기 위해 보간점 생성
            background0 = interp1(cross_range, background, query_pts);
            
            up_unwraped_phase = unwrap(angle(SAS.up.Pimage),[],2);
            down_unwraped_phase = unwrap(angle(SAS.down.Pimage),[],2);
            SR = sqrt(SAS.params.YY.^2-D^2); % slant-range
            height_est0 = (-cos(pi*fc/(2*fs))*(up_unwraped_phase + down_unwraped_phase ) /(4*pi*fc/cw*d)+0.3214) *1.1325.*SR;
            max_height_coeff = max ( max ( height_est0( :, 1:num_queries ) ) );
            scale_factor= abs(max_height_coeff / mean( background0 )) ; % empirical scale factor that seems frequency-dependent and must be found out later why it gives right estimated height.
            
            height_est = D - height_est0 * scale_factor;
            height_est = height_est - mean( mean( height_est( :, 1:num_queries ) ) );
            
        end
        
        function [ thrshdImage ] = PreprocessImage(obj, image, cutoff)
            
            preImage = image;
            thrshdImage = image.*(abs(image)>=cutoff);
            %movmeanImage = MovingAverage(thrshdImage);
            %movmeanImage = MovingAverage(obj, thrshdImage, obj.params.sas.SAS.param.XX, obj.params.delu);
            
        end
        
        function [ obj ] = MovingAverage(obj, A,XX,Res)
            
            [Nx,Ny]=size(A);
            y=zeros(Nx,Ny);

            WLeng=round(Res/(XX(2,1)-XX(1,1)))+1;
            if mod(WLeng,2)==0
                WLeng=WLeng+1;
            end

            Eind=[1:Nx]+(WLeng-1)/2;
            Sind=Eind-WLeng;

            for n=1:Ny
                for p=1:Nx
                    y(p,n)=mean(A(max(1,Sind(p)):min(Eind(p),Nx),n));
                end
            end
        end
        
        function [ obj, sas_params ] = ProcessSAS(obj, ts, data, XT, XA, sas_params)
            XX = sas_params.XX;
            YY = sas_params.YY;
            YA = 0;
            YT = 0;
            ZT = 0;
            ZA = 0;
            
            cw = obj.params.cw;
            fc = obj.params.fc;
            
            k0 = 2*pi*fc/cw;
            
            vtx = obj.params.array.vt/cw; % vtx = vtx/obj.params.cw;
            vty = 0/cw;                   % vty = vty/obj.params.cw;
            v = sqrt(vtx^2+vty^2);
            
            for idx = 1:obj.params.array.Chn
                dr = sqrt((XA(idx) - XX).^2 + (YA-YY).^2+ZA^2);
                dt = sqrt((XT(idx) - XX).^2 + (YT-YY).^2+ZT^2);
                
                dst = dt-(XX-XA(idx))*vtx-(YY-YA)*vty+sqrt(((XX-XA(idx))*vtx+(YY-YA)*vty-dt).^2-(1-v^2)*(dt.^2-dr.^2));
                dst = dst / (1-v^2);
                
                thetat=atan2(XX-XT(idx),YY-YT);
                thetar=atan2(XX-XA(idx),YY-YA);
                
                %
                
                At=sin(k0/(2*pi)*obj.params.array.D_t*sin(thetat))./(k0/(2*pi)*obj.params.array.D_t*sin(thetat)); % Tx-beam pattern
                %At=sinc(k0/(2)*obj.params.array.D_t*sin(thetat)); % Tx-beam pattern
                At(thetat==0)=1;
                
                Ar=sin(k0/(2*pi)*obj.params.array.D_ap*sin(thetar))./(k0/(2*pi)*obj.params.array.D_ap*sin(thetar)); % Rx-beam pattern
                %Ar=sinc(k0/(2)*obj.params.array.D_ap*sin(thetar)); % Rx-beam pattern
                Ar(thetar==0)=1;
                
                %tmp = abs(At.*Ar).*interp1(ts, data(idx,:), dst/cw,'spline',0).*exp(1j*2*pi*fc*dst/cw);
                tmp = abs(At.*Ar).*interp1(ts, data(idx,:), dst/cw,'linear',0).*exp(1j*2*pi*fc*dst/cw);
                sas_params.Pimage = sas_params.Pimage + tmp;
            end
            
            sas_params.Pimage = sas_params.Pimage .* YY/max(YY(1,:)); % TVG
            
        end
        
        function VernierDPCA(data, XT, XA, init)
            
            data_count = size(data, 2);
            
            if init

                NewXA = XA;
                NewXT = XT;
                
                NewVXA = (NewXA+NewXT)/2;
                
                XAA = NewXA;
                XTA = NewXT;
                VXA = NewVXA;
                
                sig_vernier = data;
                
                
                
                init = true;
            else
            end
            
        end
        
        function outputArg = SASProcessing(obj, sig, XT, XA)

            % Dt는 mean(diff(sonartime))을 알고 있으면 미리 구할 수 있음
            for idx = 1:obj.params.ping_length
                XT = XT+obj.params.array.vt*obj.params.coord.Dt(idx);
                XA = XA+obj.params.array.vt*obj.params.coord.Dt(idx);
                
            end
            
        end
    end
end

